class Button {
  constructor(props) {
    this.value = props.value || ''
    this.style = props.style || ''
    this.template = props.template || `<button>${this.value}</button>`
  }

  click(trigger) {
    return function() {
      trigger('click')
    }
  }
}

export default Button