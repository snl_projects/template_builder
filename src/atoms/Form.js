class Form {
  constructor(props) {
    this.name = props.name || ''
    this.value = props.value || ''
    this.style = props.style || ''
    this.template = props.template
    this.type = props.type || `<form name="${this.name}" type="${this.type}" action="${this.action}"></form>`
    this.action = props.action || ''
  }

  submit() {
    console.log('Form submited')
  }
}

export default Form