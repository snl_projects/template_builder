class Input {
  constructor(props) {
    this.name = props.name || ''
    this.value = props.value || ''
    this.style = props.style || ''
    this.template = props.template || `<input name="${this.name}">`
  }

  change() {
    console.log('Input change')
  }
}

export default Input