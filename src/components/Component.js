import EventEmitter from '../../vendor/EventEmitter'

class Component {
  constructor(props) {
    this.element = this.createElement(props)
    this.emitter = new EventEmitter()
  }

  createElement(props) {
    const parent  = new props.parent.class(props.parent.props)
    parent.children = {}
    props.children.forEach(item => {
      parent.children[item.name] = new item.class(item.props)
    })

    return parent
  }
}

export default Component