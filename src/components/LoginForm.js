import Button from '../atoms/Button'
import Form from '../atoms/Form'
import Input from '../atoms/Input'
import Component from './Component'


const componentConfig = {
  parent: {
    class: Form,
    props: {
      name: 'login-form'
    }
  },
  children: [
    {
      name: 'login',
      class: Input,
      props: {
        name: 'login'
      } 
    },
    {
      name: 'password',
      class: Input,
      props: {
        name: 'password'
      } 
    },
    {
      name: 'submit_btn',
      class: Button,
      props: {
        value: 'Log In'
      } 
    }
  ]
}

class LoginForm extends Component {
  constructor(props) {
    super(props)
  }
}

const FormProps = {
  name: 'login-form'
}

const Login = new LoginForm(componentConfig)

Login.emitter.on('click', () => console.log('Login form send message'))
Login.emitter.on('change', () => console.log('Login form input change'))

Login.element.children.submit_btn.click = Login.element.children.submit_btn.click(Login.emitter.trigger)

console.log(Login)

export default LoginForm