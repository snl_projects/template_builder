function EventEmitter() {
  var events = {}

  this.on = function(name, fn) {
    events[name] = events[name] || []
    events[name].push(fn)
  }

  this.trigger = function(name, args) {
    events[name] = events[name] || []
    args = args || []
    events[name].forEach(function(fn) {
      fn.apply(this, args)
    })
  }
}

export default EventEmitter